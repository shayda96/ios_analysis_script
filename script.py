import paramiko
import time

def start_test_lists(remote_connectrion):
    mass = active_list(remote_connectrion)
    i = 0
    res = {}
    while i < len(mass):
        res['list ' + mass[i]] = check_access_list(remote_connectrion, mass[i])
        i += 1
    return res

def check_access_list(remote_connection, name):

    remote_connection.send("show access-list " + name + "\n")
    time.sleep(1)
    dict = {}
    i = 0
    j = 0
    output = remote_connection.recv(52828)
    string = output.decode("utf-8")
    list = string.splitlines()
    while j < len(list) - 1:
        stri = list[0]
        if  len(stri) > 0 and stri.startswith("  ") != True:
            list.pop(0)
        if len(stri) == 0:
            list.pop(0)
        j += 1
    list.pop(len(list) - 1)
    while i < len(list):
        dict[i+1] = check_access_string(list[i])
        i+= 1
    return dict

def check_access_string(string):
    res = "ok"
    a = 10

    if "permit" in string:
        if "ip" in string:
            if string.count("any") == 1:
                res = "informational"
            if string.count("any") == 2:
                res = "Warning!"
        else:
            if string.count("any") >= 2:
                res = "Warning!"
            if string.count(".") <= 6 and res != "Warning!":
                try: last_index = string.rindex(".")
                except ValueError: a = 20
                if len(string) - last_index < 7:
                    res = "Warning!"
    return res

def active_list(remote_connection):

    remote_connection.send("show running-config | section ip access-group\n")
    time.sleep(3)
    output = remote_connection.recv(52828)
    string = output.decode("utf-8")
    list = string.splitlines()
    list.pop(len(list)-1)
    list.pop(0)
    list.pop(0)
    list.pop(0)
    i = 0
    line = ""
    res = []
    while i < len(list):
        line = list[i]
        last_index = line.rindex(" ")
        res.append(line[17:last_index])
        i += 1
    return res

def ssh_version(mass):
    i = 0
    res = "ssh disabled(informational)"
    while i < len(mass):
        if mass[i].find("ip ssh version 1") != -1:
            res = "ssh v1 you must enable ssh v2(informational)"
            break
        if mass[i].find("ip ssh version 2") != -1:
            res = "ssh v2 enabled(ok)"
            break
        i += 1
    return res

def arp_inspection(mass):
    i = 0
    res = "arp inspection disabled"
    while i < len(mass):
        if mass[i].find("ip dhcp snooping") != -1:
            res = "dhcp snooping enabled"
            if mass[i].find("ip arp inspection") != -1:
                res = "arp inspection enabled"
        i += 1
    return res



def running_conf_all(remote_connection):

    #remote_connection.send("ena\n")
    remote_connection.send("show running-config all\n")
    i = 0
    time.sleep(5)
    while i < 30:
        remote_connection.send("   ")
        i += 1
        time.sleep(0.05)

    output = remote_connection.recv(100000)
    string = output.decode("utf-8")
    mass = string.splitlines()

    return mass

def running_conf(remote_connection):

    remote_connection.send("show running-config\n")
    i = 0
    time.sleep(5)
    while i < 5:
        remote_connection.send("   ")
        i += 1
        time.sleep(0.05)

    output = remote_connection.recv(52828)
    string = output.decode("utf-8")
    mass = string.splitlines()

    return mass

def ip_route(remote_connection):


    remote_connection.send("show ip route\n")
    time.sleep(1)
    output = remote_connection.recv(52828)
    string = output.decode("utf-8")

    return string

def ssh_timeout(remote_connection):
    remote_connection.send("show ip ssh | include Authentication timeout\n")
    time.sleep(2)
    i = 0
    output = remote_connection.recv(52828)
    string = output.decode("utf-8")
    mass = string.splitlines()
    check = ""
    while i < len(mass):
        if mass[i].find("Authentication timeout:") != -1:
            check = mass[i]
            break
        i += 1
    last_index = check.rfind(" secs")
    res = "ssh_timeout = " + str(check[24:last_index]) + "secs"
    time1 = check[24:last_index]
    if int(time1) <= 120:
        res += "(ok)"
    else:
        res += "(warning)"
    return res

def line_vty(remote_connection):
    remote_connection.send("show running-config | section line vty\n")
    time.sleep(2)
    i = 0
    str = ""
    res = "line vty > 9 min"
    output = remote_connection.recv(52828)
    string = output.decode("utf-8")
    mass = string.splitlines()

    while i < len(mass):
        if mass[i].find("exec-timeout") != -1:
            str = mass[i]
            str = str[14:len(str)]
            pos = str.find(" ")
            str = str[0:pos]
            if int(str) <= 9:
                res = "line vty ok"
        i += 1
    return res

def line_con(remote_connection):
    remote_connection.send("show running-config | section line con\n")
    time.sleep(2)
    i = 0
    str = ""
    res = "line con > 9 min"
    output = remote_connection.recv(52828)
    string = output.decode("utf-8")
    mass = string.splitlines()

    while i < len(mass):
        if mass[i].find("exec-timeout") != -1:
            str = mass[i]
            str = str[14:len(str)]
            pos = str.find(" ")
            str = str[0:pos]
            if int(str) <= 9:
                res = "line con ok"
        i += 1
    return res

def small_services(conf_all):
    res = "SMALL SERVICES:\n"
    i = 0
    count = 0
    while i < len(conf_all):
        check = conf_all[i]
        if check.find("no service tcp-small-servers") != -1:
            res += "tcp-small-servers disabled\n"
            count += 1
            i += 1
            continue
        if check.find("no service udp-small-servers") != -1:
            res += "udp-small-servers disabled\n"
            count += 1
            i += 1
            continue
        if check.find("no ip finger") != -1:
            res += "ip finger disabled\n"
            count += 1
            i += 1
            continue
        if check.find("ip dhcp bootp ignore") != -1:
            res += "dhcp bootp ignore disabled\n"
            count += 1
            i += 1
            continue
        if check.find("no ip domain-lookup") != -1:
            res += "ip domain-lookup disabled\n"
            count += 1
            i += 1
            continue
        if check.find("no service pad") != -1 and res.find("service pad") == -1:
            res += "service pad disabled\n"
            count += 1
            i += 1
            continue
        if check.find("no ip http server") != -1:
            res += "ip http server disabled\n"
            count += 1
            i += 1
            continue
        if check.find("no ip http secure-server") != -1:
            res += "ip http secure-server disabled\n"
            count += 1
            i += 1
            continue
        if check.find("no service config") != -1:
            res += "service config disabled\n"
            count += 1
            i += 1
            continue
        i +=1
    res += str(9 - count) + " services are not disabled"
    return res

def logging(conf):
    res = "logging disabled"
    i = 0
    while i < len(conf):
        if conf[i].find("logging host") != -1:
            pos = conf[i].rfind(" ")
            ip = conf[i][pos+1:len(conf[i])]
            res = "logging enabled to host: " + ip
            break
        i += 1
    return res

def crashinfo(conf):
    res = "number of crashinfo files not 3(informational)"
    i = 0
    while i < len(conf):
        if conf[i].find("exception crashinfo maximum files 3") != -1:
            res = "number of crashinfo files 3 (ok)"
            break
        i += 1
    return res

def auth_fails(conf):
    res = "number of fail attempts not 3(warning)"
    i = 0
    while i < len(conf):
        if conf[i].find("aaa local authentication attempts max-fail 3") != -1:
            res = "number of fail attempts 3(ok)"
            break
        i += 1
    return res

def mode_exclusive(conf):
    res = "configuration mode exclusive disabled(informational)"
    i = 0
    while i < len(conf):
        if conf[i].find("configuration mode exclusive") != -1:
            res = "configuration mode exclusive enabled(ok)"
            break
        i += 1
    return res

def refresh_shell(ssh_client, ip_addr, usern, passwd):
    ssh_client.close()
    ssh_client.connect(hostname=ip_addr, username=usern, password=passwd)
    remote_connection = ssh_client.invoke_shell()
    remote_connection.send("ena\n")
    return remote_connection

def first_shell(ssh_client, ip_addr, usern, passwd):
    ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh_client.connect(hostname=ip_addr, username=usern, password=passwd)
    remote_connection = ssh_client.invoke_shell()
    remote_connection.send("ena\n")
    return remote_connection

def test_router(ssh_client):
    print("Введіть IP адресу")
    ip_addr = input()
    print("Введіть username")
    usern = input()
    print("Введіть password")
    passwd = input()
    remote_connection = first_shell(ssh_client, ip_addr, usern, passwd)
    print("Checking active access-lists:")
    print(start_test_lists(remote_connection))
    remote_connection = refresh_shell(ssh_client, ip_addr, usern, passwd)
    conf = running_conf(remote_connection)
    remote_connection = refresh_shell(ssh_client, ip_addr, usern, passwd)
    conf_all = running_conf_all(remote_connection)
    remote_connection = refresh_shell(ssh_client, ip_addr, usern, passwd)
    print(ssh_version(conf))
    print(ssh_timeout(remote_connection))
    remote_connection = refresh_shell(ssh_client, ip_addr, usern, passwd)
    print(line_con(remote_connection))
    remote_connection = refresh_shell(ssh_client, ip_addr, usern, passwd)
    print(line_vty(remote_connection))
    print(small_services(conf_all))
    print(logging(conf))
    print(crashinfo(conf))
    print(auth_fails(conf))
    print(mode_exclusive(conf))
    return

def main():
    ip_addr = "192.168.1.240"
    usern = "cisco"
    passwd = "cisco"

    ssh_client = paramiko.SSHClient()
    remote_connection = first_shell(ssh_client, ip_addr, usern, passwd)

    #conf_all = running_conf_all(remote_connection)

    #remote_connection = refresh_shell(ssh_client, ip_addr, usern, passwd)
    conf = running_conf(remote_connection)
    print(conf)
    test_router(ssh_client)
    ssh_client.close()


main()